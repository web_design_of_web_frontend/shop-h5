'use strict'
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
function DataTimeFormat() {
  let fmt = "-yyyyMMdd-HHmm"
  let netime = new Date()
  let o = {
    'M+': netime.getMonth() + 1, // 月份
    'd+': netime.getDate(), // 日
    'H+': netime.getHours(), // 小时
    'm+': netime.getMinutes(), // 分
    's+': netime.getSeconds(), // 秒
    'q+': Math.floor((netime.getMonth() + 3) / 3), // 季度
    'S': netime.getMilliseconds() // 毫秒
  }
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (netime.getFullYear() + '').substr(4 - RegExp.$1.length))
  for (let k in o) { if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)) }
  return fmt
}

const VERSION = "V0.1.0" // 版本号
const timeTag = DataTimeFormat()

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  /**
   * You will need to set publicPath if you plan to deploy your site under a sub path,
   * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
   * then publicPath should be set to "/bar/".
   * In most cases please use '/' !!!
   * Detail: https://cli.vuejs.org/config/#publicpath
   */
  publicPath: './',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: 9528,
    open: false,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: 'http://192.168.137.150:10120'
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    resolve: {
      alias: {
        '@': resolve('src')
      }
    },
    output: { // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
      filename: `static/js/[name].${VERSION}.${timeTag}.js`,
      chunkFilename: `static/js/[name].${VERSION}.${timeTag}.js`
    },
  },
  chainWebpack(config) {
    config.plugins.delete('preload') // TODO: need test
    config.plugins.delete('prefetch') // TODO: need test

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  },

  css: {
    loaderOptions: {
      less: {
        // 若使用 less-loader@5，请移除 lessOptions 这一级，直接配置选项。
        lessOptions: {
          modifyVars: {
            // 通过 less 文件覆盖（文件路径为绝对路径）
            hack: `true; @import "src/css/anVant.less";`,
          },
        },
      },
    },
  },

}