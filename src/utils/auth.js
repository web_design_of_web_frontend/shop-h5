import store from '@/store';
import { Toast } from 'vant';
import * as Api from "@/api/data";

// 数组去重
export function uniqueArray(val) {
  return val.filter((v, i) => val.indexOf(v) === i);
}

/* 生成验证码的函数  length验证码长度  type 验证码类型 */
export function createCode(length,type) {
  let code="";
  let selectChar=new Array(1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
  let numberChar = [1,2,3,4,5,6,7,8,9];
  for(let i=0;i<length;i++) {
    if(type === 'number') {
      let charIndex=Math.floor(Math.random()*9);
      code += numberChar[charIndex];
    }else{
      let charIndex=Math.floor(Math.random()*61);
      code += selectChar[charIndex];
    }
  }
  return code;
}

/* 对象是否为空 */
export function isEmpty(obj) {
  if (typeof obj == 'number') {
    obj = obj.toString();
  }
  // 本身为空直接返回true
  if (obj == null) return true;
  // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  // 最后通过属性长度判断。
  for (let key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

/*
  加入购物车
  @goods 商品参数
  @activeOption 选中的商品规格
*/
export function addCart(goods, activeOption, number=1) {
  if(store.state.isLogin) {
    // 如果是登录状态 直接提交数据添加购物车
    let cart = [{
      goodsID: goods.id,
      goodsNum: number,
      standardID: activeOption.id
    }];
    let nd = {
      userID: store.state.userInfo.id,
      attrValues: encodeURI(JSON.stringify(cart))
    };
    Api.postCart(nd).then(() => {
      Toast.success('成功加入购物车');
      // 更新购物车商品数量
      store.dispatch('getCartNumber');
    });
    return;
  }
  // 非登录状态下
  // 先检查原购物车中是否有该商品数据
  let list = store.state.cart;
  let isExist = false; // 是否存在 默认不存在
  list.forEach(item => {
    if(item.goodsID == goods.id) {
      // 如果存在规格  判断选择的规格是否相同
      if(item.goodsStandard && activeOption.id) {
        if(item.goodsStandard == activeOption.id) {
          // 如果是相同规格商品 并且库存足够的情况下 增加数量
          if(item.goodsNum < activeOption.stock) {
            item.goodsNum = item.goodsNum + number;
          }
        }else{
          // 规格不同 添加购物车
          list.push(addCartChange(goods, activeOption.id, number));
        }
      }else{
        // 没有多选规格 只有默认一个
        item.goodsNum = item.goodsNum + number;
      }
      isExist = true;
    }
  }); // 存在两种规格 多次添加情况 需要去重

  // 如果不存在 添加
  if(!isExist) {
    list.push(addCartChange(goods, activeOption.id, number));
  }

  // 去重
  let nd = [];
  for(let i = 0; i < list.length; i++) {
    for(let j = i+1; j < list.length; j++) {
      // 商品ID相同 规格相同 去重
      if(list[i].goodsID == list[j].goodsID && list[i].goodsStandard == list[j].goodsStandard) {
        list[j].goodsNum = list[i].goodsNum + list[j].goodsNum;
        ++i;
      }
    }
    nd.push(list[i]);
  }
  // 存储修改后的数据
  store.dispatch('addCart', nd);
  Toast.success('成功加入购物车');
}

// 购物车数据转换
export function addCartChange(goods, standardID, number, id) {
  let standard,
    options,
    price,
    stock,
    standardName;
  if(goods.options) {
    options = JSON.parse(goods.options);
    options.forEach(item => {
      if(item.id == standardID) {
        standard = item;
        standardName = item.name;
      }
    });
  }
  price = standard?standard.price:goods.price;
  stock = standard?standard.stock:goods.stock;
  return {
    id: id?id:Date.parse(new Date()),
    goodsID: goods.id,
    goodsNum: number, // 商品数量
    goodsPrice: parseFloat(price).toFixed(2), // 商品价格
    standardName: standardName, // 所选规格名称
    stock:stock, // 库存
    userID: '',
    checked: true,
    goods: goods,
    standardID: standardID?standardID:'' // 所选规格ID
  };
}

// 保留两位小数 自动补零
export function returnFloat(num) {
  let value=Math.round(parseFloat(num)*100)/100;
  let xsd=value.toString().split(".");
  if(xsd.length==1) {
    value=value.toString()+".00";
    return value;
  }
  if(xsd.length>1) {
    if(xsd[1].length<2) {
      value=value.toString()+"0";
    }
    return value;
  }
}


/*
  加入收藏
  @goods 商品参数
  @delete 1为删除
*/
export function addCollect(goodsID, del=0) {
  return new Promise((resolve, reject) => {
    if(store.state.isLogin) {
      // 如果是登录状态 直接提交数据添加购物车
      let nd = {
        userID: store.state.userInfo.id,
        goodsID: goodsID,
        goodsNum: 1,
        isCollect:1
      };
      if(del == 0) {
        Api.addCart(nd).then(() => {
          Toast.success('收藏成功！');
          resolve();
        });
      }else{
        // 删除收藏
        Api.cartDelete(nd).then(() => {
          resolve();
        });
      }
    }else{
      Toast('请先登录');
      reject();
    }
  });
}