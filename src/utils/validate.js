/* 特殊字符*/
export function isvalidUsername (str) {
  const regName = /[~#^$@%&!*()<>:;'"{}【】 {2}]/;
  return regName.test(str.trim());
}

/* 合法uri*/
export function validateURL (textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return urlregex.test(textval);
}

/* 小写字母*/
export function validateLowerCase (str) {
  const reg = /^[a-z]+$/;
  return reg.test(str);
}

/* 大写字母*/
export function validateUpperCase (str) {
  const reg = /^[A-Z]+$/;
  return reg.test(str);
}

/* 大小写字母*/
export function validatAlphabets (str) {
  const reg = /^[A-Za-z]+$/;
  return reg.test(str);
}

/* 数字*/
export function validatNum (str) {
  const reg = /^[0-9]+$/;
  return reg.test(str);
}

/* IP  */
export function validatIP (str) {
  const re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;// 正则表达式
  if (re.test(str)) {
    if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256) { return true; }
  }
  return false;
}

/* 手机号*/
export function validatPhone(str) {
  const reg = /^1[3456789]\d{9}$/;
  return reg.test(str);
}

/* 邮箱 */
export function validaEmail(val) {
  let reg = /[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
  return reg.test(val);
}

/*  是否为正整数 */
export function isPositiveInteger (s) {
  let re = /^[0-9]+$/;
  return re.test(s);
}

/*  是否为传真 格式 区号-电话(固话)/传真号码-分机号*/
export function isFax (s) {
  let re = /^(0?\d{2,3}\-)?[1-9]\d{6,7}(\-\d{1,4})?$/;
  return re.test(s);
}

/* 非空 */
export function isEmptyValue (obj) {
  // 本身为空直接返回true
  if (obj == null) return true;
  // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  // 最后通过属性长度判断。
  for (let key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }

  return true;
}

/* IP段 逗号分隔验证  */
export function isIpList (ips) {
  let reg = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;

  let valdata = ips.split(',');
  let nd = true;
  for (let i = 0; i < valdata.length; i++) {
    if (reg.test(valdata[i]) == false) {
      nd = false;
    }
  }
  return nd;
}


