import axios from 'axios';
import { Notify } from 'vant';
import store from '../store';

// 创建axios实例
const service = axios.create({
  retry: 0, // 最大请求次数
  timeout: 30 * 1000 // 请求超时时间
});

// request拦截器
service.interceptors.request.use(config => {
  if (config.url.indexOf('/shop/') == -1) {
    config.url = '/shop' + config.url;
  }
  return config;
}, error => {
  // Do something with request error
  console.log(error); // for debug
  Promise.reject(error);
});

// respone拦截器
service.interceptors.response.use(
  response => {
  /**
  * code为非1是抛错
  */
    const res = response.data;
    if (res.code !== 1) {
      // -999:授权失败重新登录;
      if (res.code === -999) {
        store.dispatch('getInfo'); // 获取用户信息
        // 重新发起请求
        return service.request(response.config);
      }
      Notify(res.msg === '' ? '服务器错误' : res.msg);
      return Promise.reject('error');
    } else {
      return res.result;
    }
  },
  error => {
    // 如果连接超时会再次请求，直到超过最多请求次数
    let config = error.config;
    // If config does not exist or the retry option is not set, reject
    if (!config || !config.retry) return Promise.reject(error);

    // 设置请求次数
    config.__retryCount = config.__retryCount || 0;

    // 是否超过最大请求次数
    if (config.__retryCount >= config.retry) {
      // 报错
      console.log('错误：' + error);// for debug
      Notify('网络故障');
      return Promise.reject(error);
    }

    // 请求次数自增
    config.__retryCount += 1;
    console.log('再次请求：' + config.__retryCount);

    // 设置定时器 超过请求时间后重新请求
    let backoff = new Promise(function (resolve) {
      setTimeout(function () {
        resolve();
      }, config.timeout || 1);
    });

    // Return the promise in which recalls axios to retry the request
    return backoff.then(function () {
      return service.request(config);
    });
  }
);


export default service;
