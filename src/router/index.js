import { createRouter, createWebHashHistory } from 'vue-router';
import Layout from '@/components/Layout';

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: Layout,
    children:[
      {
        path: '/home',
        name: 'Home',
        component: () => import('../views/home'),
        meta: { title: '首页', keepAlive: true}
      },
      {
        path: '/sort',
        name: 'Sort',
        props: (route) => ({ tab: route.query.tab }),
        component: () => import('../views/sort'),
        meta: { title: '分类', keepAlive: true}
      },
      {
        path: '/cart',
        name: 'Cart',
        component: () => import('../views/cart'),
        meta: { title: '购物车'}
      },
      {
        path: '/user',
        name: 'User',
        component: () => import('../views/user'),
        meta: { title: '用户中心', keepAlive: true},
        children:[
          {
            path: '/address',
            name: 'address',
            props: (route) => ({ flag: route.query.flag }),
            component: () => import('../views/address'),
            meta: { title: '地址管理'}
          }, {
            path: '/addressEdit',
            name: 'addressEdit',
            props: (route) => ({ prom: route.query.prom }),
            component: () => import('../views/address/addressEdit'),
            meta: { title: '编辑地址', keepAlive: true}
          },{
            path: '/userSys',
            name: 'userSys',
            component: () => import('../views/user/page/userSys'),
            meta: { title: '设置', keepAlive: true}
          },{
            path: '/editPassword',
            name: 'editPassword',
            component: () => import('../views/user/page/editPassword'),
            meta: { title: '修改密码', keepAlive: true}
          }
        ]
      }
    ]
  },{
    path: '/goods',
    name: 'Goods',
    props: (route) => ({ id: route.query.id }),
    component: () => import('../views/goods'),
    meta: { title: '商品详情', keepAlive: true}
  },{
    path: '/search',
    name: 'Search',
    component: () => import('../views/search'),
    meta: { title: '搜索'}
  },{
    path: '/order',
    name: 'Order',
    props: (route) => ({ prom: route.query.prom }),
    component: () => import('../views/order'),
    meta: { title: '确认订单'}
  },{
    path: '/orderList',
    name: 'OrderList',
    props: (route) => ({ tab: route.query.tab }),
    component: () => import('../views/order/orderList'),
    meta: { title: '订单管理'}
  },{
    path: '/orderInfo',
    name: 'OrderInfo',
    props: (route) => ({ id: route.query.id }),
    component: () => import('../views/order/orderInfo'),
    meta: { title: '订单详情'}
  },{
    path: '/play',
    name: 'play',
    props: (route) => ({ prom: route.query.prom }),
    component: () => import('../views/order/play'),
    meta: { title: '支付界面'}
  },{
    path: '/playSuccess',
    name: 'playSuccess',
    component: () => import('../views/order/playSuccess'),
    meta: { title: '支付成功'}
  },{
    path: '/collect',
    name: 'collect',
    component: () => import('../views/collect'),
    meta: { title: '我的收藏'}
  },{
    path: '/login',
    name: 'login',
    component: () => import('../views/login'),
    meta: { title: '登录'}
  },{
    path: '/register',
    name: 'Register',
    props: (route) => ({ tab: route.query.tab }),
    component: () => import('../views/login/register'),
    meta: { title: '注册'}
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
