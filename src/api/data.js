import request from '@/utils/request';
import store from '@/store';

// eslint-disable-next-line no-undef
const qs = require('qs');

// 注册
export function register(d) {
  return request({
    url: '/sys/user/register',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 登录
export function login(d) {
  return request({
    url: '/sys/user/login',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 检查用户名 邮箱 手机号 是否被占用
export function checkUser(d) {
  return request({
    url: '/sys/user/check',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 修改密码
export function userUpdate(d) {
  return request({
    url: '/sys/user/update',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 查询商品分类
export function goodsTypeList() {
  return request({
    url: '/sys/goodsType/list',
    method: 'get',
    params: ''
  });
}

// 查询商品列表
export function goodsList(d) {
  return request({
    url: '/sys/goods/list',
    method: 'get',
    params: d
  });
}

// 查询商品详情
export function goodsInfo(id) {
  return request({
    url: `/sys/goods/${id}`,
    method: 'get',
    params: ''
  });
}

// 查询促销商品
export function indexSales() {
  return request({
    url: '/sys/indexSales',
    method: 'get',
    params: {state:1}
  });
}

// 查询购物车
export function getCart(d) {
  return request({
    url: '/sys/cart',
    method: 'get',
    params: d
  });
}

// 查询购物车中商品数量
export function getCartNumber(userID) {
  return request({
    url: '/sys/cart/number',
    method: 'get',
    params: {userID: userID}
  });
}

// 新增 修改购物车
export function postCart(d) {
  return request({
    url: '/sys/cart',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 查询商品规格
export function goodsSpec() {
  return request({
    url: '/sys/goodsSpec',
    method: 'get',
    params: ''
  });
}

// 删除购物车商品
export function deleteCart(ids) {
  return request({
    url: '/sys/cart',
    method: 'delete',
    params: {ids: ids}
  });
}

// 查询是否收藏该商品
export function includeCart(d) {
  return request({
    url: '/sys/cart/include',
    method: 'get',
    params: d
  });
}

// 添加收藏/购物车
export function addCart(d) {
  return request({
    url: '/sys/cart/add',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 根据商品ID删除收藏/购物车
export function cartDelete(d) {
  return request({
    url: '/sys/cart/delete',
    method: 'get',
    params: d
  });
}

// 查询收货地址列表
export function userAddressList(d) {
  return request({
    url: '/sys/userAddress/list',
    method: 'get',
    params: d
  });
}

// 新增或修改收货地址
export function userAddressAdd(d) {
  return request({
    url: '/sys/userAddress/add',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 删除收货地址
export function userAddressDelete(id) {
  return request({
    url: '/sys/userAddress/delete',
    method: 'get',
    params: {id:id}
  });
}

// 查询默认收货地址
export function getDefAddress() {
  return request({
    url: '/sys/userAddress/defAddress',
    method: 'get',
    params: {userID: store.state.userInfo.id}
  });
}

// 查询运费表
export function getFreight() {
  return request({
    url: '/sys/freight',
    method: 'get',
    params: ''
  });
}

// 新增订单
export function orderAdd(d) {
  return request({
    url: '/sys/order/add',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 查询订单列表
export function orderList(d) {
  return request({
    url: '/sys/order/list',
    method: 'get',
    params: d
  });
}

// 订单详情
export function orderInfo(id) {
  return request({
    url: `/sys/order/${id}`,
    method: 'get',
    params: ''
  });
}

// 修改订单状态
export function orderUpdate(d) {
  return request({
    url: '/sys/order/update',
    method: 'post',
    data: qs.stringify(d)
  });
}

/*
  订单状态
千位 1000待付款，1100待发货，1110待收货，1120待评价，2000交易完成
-4000：交易失败
-3000：系统取消
-2000：卖家取消
-1000：买家取消
1000：创建，初始化状态
2000：交易完成
百位
100：已支付
十位
10：已发货
20:确认收货
个位
1：买家已评论
2：卖家已评论
3：双方已评论 */
export const orderState ={
  1000: '待付款',
  1100: '待发货',
  1110: '已发货',
  1120: '待评价',
  2000: '交易完成'
};