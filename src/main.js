import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import Vant from 'vant';
import 'vant/lib/index.css';
import 'vant/lib/index.less';

import '@/icons'; // icon
import '@/css/anstyle.scss';

const app = createApp(App);

import SvgIcon from '@/components/SvgIcon'; // svg组件
// 注册全局组件
app.component('svg-icon', SvgIcon);

import * as api from "@/api/data";
import * as auth from "@/utils/auth";
// 配置全局属性
app.config.globalProperties.$api = api;
app.config.globalProperties.$auth = auth;

app.use(store)
  .use(router)
  .use(Vant)
  .mount('#app');
